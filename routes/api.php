<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function(){
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('getUser', 'Api\AuthController@getUser');

        // Addresses
        Route::get('address/{id}', 'Api\AddressController@index');
        Route::post('address', 'Api\AddressController@store');
        Route::put('address/{id}', 'Api\AddressController@update');
        Route::delete('address/{id}', 'Api\AddressController@delete');

        // Shipping
        Route::get('shipping/{id}', 'Api\ShippingController@index');
        Route::post('shipping', 'Api\ShippingController@store');
        Route::put('shipping/{id}', 'Api\ShippingController@update');
        Route::delete('shipping/{id}', 'Api\ShippingController@delete');

        // Orders
        Route::get('order/{id}', 'Api\OrderController@index');
        Route::post('order', 'Api\OrderController@store');
        Route::put('order/{id}', 'Api\OrderController@update');
        Route::delete('order/{id}', 'Api\OrderController@delete');

        // Product
        Route::get('product', 'Api\ProductController@index');
        Route::post('product', 'Api\ProductController@store');
        Route::put('product/{id}', 'Api\ProductController@update');
        Route::delete('product/{id}', 'Api\ProductController@delete');

    });
});
