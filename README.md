## Api documentation

```
+----------+----------------------+------------------------------------+
| Method   | URI                  | Params                             |
+----------+----------------------+------------------------------------+
|          OPEN ENDPOINTS                                              |
| POST     | api/v1/register      | 'name' => 'required',              |
|                                 | 'email' => 'required|email',       |
|                                 | 'password' => 'required',          |
|                                 | 'confirm_password' => 'required',  |
|                                 |  RETURN                            |
|                                 |  'api_token' => 'string'           |
+----------+----------------------+------------------------------------+
| POST     | api/v1/login         | 'name' => 'required',              |
|                                 | 'email' => 'required|email',       |
|                                 |  RETURN                            |
|                                 |  'api_token' => 'string'           |
+----------+----------------------+------------------------------------+
|          LOKED ENDPOINTS                                             |
| GET      | api/v1/getUser       |  'api_token' => 'string'           |
|                                 |  RETURN                            |
|                                 |  'user' => 'object'                |
|                                 |  'addresses' => 'object.address'   |
|                                 |  'orders' => 'object.orders'       |
+----------+----------------------+------------------------------------+
|          ADDRESSESS ENDPOINTS                                        |
| POST     | api/v1/address       |  'api_token' => 'string',          |
|                                 |  RETURN                            |
|                                 |  'addresses' => 'address'          |
+----------+----------------------+------------------------------------+
| GET|HEAD | api/v1/address/{id}  |  'api_token' => 'string',          |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 |  'addresses' => 'object'           |
+----------+----------------------+------------------------------------+
| DELETE   | api/v1/address/{id}  |  'api_token' => 'string',          |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 |  'addresses' => 'object'           |
+----------+----------------------+------------------------------------+
| PUT      | api/v1/address/{id}  |  'api_token' => 'string',          |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 |  'addresses' => 'object'           |
+----------+-------------------------------+---------------------------+
|          ORDERS ENDPOINTS                                            |
| POST     | api/v1/order         |  'api_token' => 'string'           |
|                                 |  RETURN                            |
|                                 |  'orders' => 'object'              |
+----------+----------------------+------------------------------------+
| DELETE   | api/v1/order/{id}    |  'api_token' => 'string'           |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 |  'orders' => 'object'              |
+----------+----------------------+------------------------------------+
| PUT      | api/v1/order/{id}    |  'api_token' => 'string'           |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 |  'orders' => 'object'              |
+----------+----------------------+------------------------------------+
| GET|HEAD | api/v1/order/{id}    |  'api_token' => 'string'           |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 |  'orders' => 'object'              |
+----------+----------------------+------------------------------------+
|          PRODUCTS ENDPOINTS                                          |
| GET|HEAD | api/v1/product       |  'api_token' => 'string'           |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 |  'products' => 'object'            |
+----------+----------------------+------------------------------------+
| POST     | api/v1/product       |  'api_token' => 'string'           |
|                                 |  RETURN                            |
|                                 |  'products' => 'object'            |
+----------+----------------------+------------------------------------+
| PUT      | api/v1/product/{id}  |  'api_token' => 'string'           |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 |  'products' => 'object'            |
+----------+----------------------+------------------------------------|
| DELETE   | api/v1/product/{id}  |  'api_token' => 'string'           |
|                                 |  RETURN                            |
|                                 |  'products' => 'object'            |
+----------+----------------------+------------------------------------|
|         SHIPPINGS ENDPOINTS                                          |
| POST     | api/v1/shipping      | 'api_token' => 'string'            |
|                                 |  RETURN                            |
|                                 | 'shipping' => 'object'             |
+----------+----------------------+------------------------------------|  
| GET|HEAD | api/v1/shipping/{id} | 'api_token' => 'string'            |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 | 'shipping' => 'object'             |
+----------+----------------------+------------------------------------|  
| PUT      | api/v1/shipping/{id} | 'api_token' => 'string'            |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 | 'shipping' => 'object'             |
+----------+----------------------+------------------------------------|  
| DELETE   | api/v1/shipping/{id} | 'api_token' => 'string'            |
|                                 |  'id' => 'number',                 |
|                                 |  RETURN                            |
|                                 | 'shipping' => 'object'             |
+----------+----------------------+------------------------------------+
```

## Steps to deploy
```git clone ...
composer install
cp .env.explame .env
set values on .env
php artisan key:generate
php artisan migrate --seed
php artisa passport:install
php artisan serve
```
#### Default credentials 
> email: helg18@gmail.com
> password: secret


## Dev Support

- **[telegra](https://t.me/helg18)**
- **[twitter](https://twitter.com/helg18)**
- **[whastapp](https://wa.me/584126539733)**
- **[email](mailto:helg18@gmail.com)**
