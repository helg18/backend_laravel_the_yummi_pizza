<?php
use Illuminate\Database\Seeder;
use App\Repositories\UserRepository;

class UserSeeder extends Seeder
{
    /**
 * @var UserRepository
 */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->repository->create([
            'name' => 'Henry Leon',
            'email' => 'helg18@gmail.com',
            'password' => bcrypt('secret')
        ]);
    }
}
