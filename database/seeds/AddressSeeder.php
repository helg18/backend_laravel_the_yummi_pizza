<?php
use App\Repositories\AddressRepository;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * @var AddressRepository
     */
    private $repository;

    /**
     * AddressSeeder constructor.
     * @param AddressRepository $repository
     */
    public function __construct(AddressRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->repository->create([
            'address' => '42 Wallaby Way, Sydney',
            'user_id' => 1
        ]);
    }
}
