<?php

use App\Repositories\ShippingRepository;
use Illuminate\Database\Seeder;

class ShippingSeeder extends Seeder
{

    /**
     * @var ShippingRepository
     */
    private $repository;

    public function __construct(ShippingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->repository->create([
            'shipping' => 'FedEx',
            'price_usd' => 5,
            'price_eur' => 6
        ]);
        $this->repository->create([
            'shipping' => 'USPS',
            'price_usd' => 6,
            'price_eur' => 7
        ]);
        $this->repository->create([
            'shipping' => 'DHL',
            'price_usd' => 7,
            'price_eur' => 8
        ]);
    }
}
