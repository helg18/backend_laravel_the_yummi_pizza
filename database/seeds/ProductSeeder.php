<?php

use App\Repositories\ProductRepository;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * @var ProductRepository
     */
    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->repository->create([
            'name' => 'Neapolitan Pizza',
            'img_url' => 'https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_623344781.jpg',
            'price_usd' => 5,
            'price_eur' => 6
        ]);

        $this->repository->create([
            'name' => 'California style pizza ',
            'img_url' => 'https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_570541132.jpg',
            'price_usd' => 5,
            'price_eur' => 6
        ]);

        $this->repository->create([
            'name' => 'Chicago Deep Dish Pizza',
            'img_url' => 'https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_514457074.jpg',
            'price_usd' => 5,
            'price_eur' => 6
        ]);

        $this->repository->create([
            'name' => 'Chicago Thin Crust Pizza',
            'img_url' => 'https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_548340295.jpg',
            'price_usd' => 5,
            'price_eur' => 6
        ]);

        $this->repository->create([
            'name' => 'Detroit Style Pizza',
            'img_url' => 'https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_347791016.jpg',
            'price_usd' => 5,
            'price_eur' => 6
        ]);

        $this->repository->create([
            'name' => 'New England Greek Style Pizza',
            'img_url' => 'https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_184944413.jpg',
            'price_usd' => 5,
            'price_eur' => 6
        ]);

        $this->repository->create([
            'name' => 'New York Thin Crust Pizza',
            'img_url' => 'https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_313437680.jpg',
            'price_usd' => 5,
            'price_eur' => 6
        ]);

        $this->repository->create([
            'name' => 'Coke 1L',
            'img_url' => 'https://i0.pngocean.com/files/574/913/210/caffeine-free-coca-cola-soft-drink-coca-cola-thumb.jpg',
            'price_usd' => 1,
            'price_eur' => 1.5
        ]);

        $this->repository->create([
            'name' => 'Coke 2L',
            'img_url' => 'https://img1.freepng.es/20171220/ree/coca-cola-bottle-png-image-5a3ac111117330.71397511151379995307157705.jpg',
            'price_usd' => 2,
            'price_eur' => 2.3
        ]);

    }
}
