<?php

use App\Models\Order;
use App\Repositories\OrderRepository;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * @var OrderRepository
     */
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Order::class, 10)->create();
    }
}
