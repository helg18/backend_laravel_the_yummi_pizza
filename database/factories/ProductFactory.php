<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $price = rand(1, 10);
    return [
        'name' => $faker->userName,
        'price_usd' => $price,
        'price_eur' => $price * 1.2
    ];
});
