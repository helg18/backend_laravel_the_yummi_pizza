<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    $price = rand(1, 10);
    return [
        'user_id' => 1,
        'total_amount_usd' => $price,
        'total_amount_eur' => $price * 1.2,
        'shipping_id' => rand(1, 3),
        'address_id' => 1,
        'status' => $faker->boolean,
    ];
});
