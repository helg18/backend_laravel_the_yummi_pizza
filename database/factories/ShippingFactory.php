<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Shipping;
use Faker\Generator as Faker;

$factory->define(Shipping::class, function (Faker $faker) {
    $price = rand(1, 10);
    return [
        'shipping' => $faker->name,
        'price_usd' => $price,
        'price_eur' => $price * 1.2
    ];
});
