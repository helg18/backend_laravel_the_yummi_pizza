<?php
namespace App\Repositories;

use App\User;
use Illuminate\Support\Collection;

/**
 * Class UserRepository
 *
 * @package App\Repositories
 */
class UserRepository extends AbstractRepository
{
    /**
     * AddressRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Auxiliary method to build complex queries.
     *
     * @param Collection $joins
     * @param $table
     * @param $first
     * @param $second
     * @param string $join_type
     */
    private function addJoin(Collection &$joins, $table, $first, $second, $join_type = 'inner')
    {
        if (!$joins->has($table)) {
            $joins->put($table, json_encode(compact('first', 'second', 'join_type')));
        }
    }

    /**
     * Main Method to search records into DB
     *
     * @param array $params
     * @param bool $count
     * @param bool $distinct
     * @return mixed
     */
    public function search($params = [], $count = false, $distinct = true)
    {
        $joins = collect();

        $query = $this->model
            ->select('users.*');

        if ($distinct) {
            $query = $query->distinct();
        }

        $joins->each(function ($item, $key) use (&$query) {
            $item = json_decode($item);
            $query->join($key, $item->first, '=', $item->second, $item->join_type);
        });

        return $query->get();
    }

}
