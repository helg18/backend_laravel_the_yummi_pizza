<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Shipping;
use App\Repositories\ShippingRepository;
use App\Traits\JsonResponse;
use Exception;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    use JsonResponse;
    /**
     * @var ShippingRepository
     */
    private $repository;

    /**
     * ShippingController constructor.
     * @param ShippingRepository $repository
     */
    public function __construct(ShippingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $shipping = $this->repository->search()->toArray();
        return self::success($shipping);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $shipping = $this->repository->create($request->all());
        return self::resourceCreated($shipping->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Shipping $shipping)
    {
        $shipping = $this->repository->updateOrCreate($shipping->toArray(), $request->all());
        return self::resourceUpdated($shipping->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Shipping $shipping)
    {
        try {
            $response = $this->repository->delete($shipping);
        } catch (Exception $e) {
            return self::errorResponse($e->getMessage(), $e->getCode());
        }
        return self::success($response);
    }
}
