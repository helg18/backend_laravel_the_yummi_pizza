<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Traits\JsonResponse;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    use JsonResponse;

    /**
     * @var OrderRepository
     */
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $order = $this->repository->search()->toArray();
        return self::success($order);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'total_amount_usd' => 'required|numeric|min:1',
                'total_amount_eur' => 'required|numeric|min:1',
                'shipping_id' => 'required|exists:shippings,id',
                'address_id' => 'required|exists:addresses,id',
                'product_id' => 'required|array',
                'product_id.*' => 'exists:product,id'
            ]);
        if ($validator->fails()) {
            self::badRequest($validator->errors());
        }

        try {
            DB::beginTransaction();

            $order = $this->repository->create([
                'user_id' => $request['user_id'],
                'total_amount_usd' => $request->total_amount_usd,
                'total_amount_eur' => $request->total_amount_eur,
                'shipping_id' => $request->shipping_id,
                'address_id' => $request->address_id,
                'product_id' => 'required|array',
                'product_id.*' => 'exists:product,id'
            ]);

            $order->products()->sync($request->product_id);

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            logger($exception->getCode() . " " + $exception->getMessage());
            self::errorResponse($exception->getMessage(), $exception->getCode());
        }
        return self::resourceCreated($order->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Order $order)
    {
        $validator = Validator::make($request->all(),
        [
            'user_id' => 'required|exists:users,id',
            'total_amount_usd' => 'required|numeric|min:1',
            'total_amount_eur' => 'required|numeric|min:1',
            'shipping_id' => 'required|exists:shippings,id',
            'address_id' => 'required|exists:addresses,id',
            'product_id' => 'required|array',
            'product_id.*' => 'exists:product,id'
        ]);
        if ($validator->fails()) {
            self::badRequest($validator->errors());
        }

        $order = $this->repository->updateOrCreate($order->toArray(), $request->all());
        return self::resourceUpdated($order->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Order $order)
    {
        try {
            $response = $this->repository->delete($order);
        } catch (Exception $e) {
            return self::errorResponse($e->getMessage(), $e->getCode());
        }
        return self::success($response);
    }
}
