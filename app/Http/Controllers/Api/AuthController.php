<?php
namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public $successStatus = 200;

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message'  => $validator->errors()
            ], $this->successStatus, [], JSON_PRETTY_PRINT);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('AppName')->accessToken;
        return response()->json([
            'error' => false,
            'data'  => $success
        ], $this->successStatus, [], JSON_PRETTY_PRINT);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email',
                'password' => 'required'
            ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message'  => $validator->errors()
            ], $this->successStatus, [], JSON_PRETTY_PRINT);
        }
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('AppName')->accessToken;
            return response()->json([
                'error' => false,
                'data'  => $success
            ], $this->successStatus, [], JSON_PRETTY_PRINT);
        } else {
            return response()->json([
                'error' => true,
                'message'  => "Invalid credentials"
            ], $this->successStatus, [], JSON_PRETTY_PRINT);
        }
    }

    public function getUser()
    {
        $user = Auth::user()->load([
            'addresses',
            'orders',
            'orders.products',
            'orders.shipping',
            'orders.address'
        ]);
        return response()->json([
            'error' => false,
            'data'  => $user
        ], $this->successStatus, [], JSON_PRETTY_PRINT);
    }
}
