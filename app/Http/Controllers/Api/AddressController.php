<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Repositories\AddressRepository;
use App\Traits\JsonResponse;
use Exception;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    use JsonResponse;

    /**
     * @var AddressRepository
     */
    private $repository;

    public function __construct(AddressRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $address = $this->repository->search()->toArray();
        return self::success($address);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $address = $this->repository->create($request->all());
        return self::resourceCreated($address->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Address $address)
    {
        $address = $this->repository->updateOrCreate($address->toArray(), $request->all());
        return self::resourceUpdated($address->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Address $address)
    {
        try {
            $response = $this->repository->delete($address);
        } catch (Exception $e) {
            return self::errorResponse($e->getMessage(), $e->getCode());
        }
        return self::success($response);
    }
}
