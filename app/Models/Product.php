<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=[
        'name', 'price_usd', 'price_eur', 'img_url'
    ];

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withTimestamps()->withPivot(['quantity']);
    }
}
