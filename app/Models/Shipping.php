<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $fillable = [
        'shipping',
        'price_usd',
        'price_eur'
    ];

    public function orders() {
        return $this->hasMany(Order::class);
    }
}
